package id.co.pkp.employee;

import id.co.pkp.employee.model.Employee;
import id.co.pkp.employee.repository.EmployeeRepository;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@OpenAPIDefinition(info =
@Info(title = "Employee API", version = "1.0", description = "Documentation Employee API v1.0")
)
@EnableDiscoveryClient
public class EmployeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeApplication.class, args);
    }

    @Bean
    EmployeeRepository employeeRepository() {
        EmployeeRepository repository = new EmployeeRepository();
        repository.add(new Employee(1L, 1L, "Uzumaki Naruto", 34, "Analyst"));
        repository.add(new Employee(1L, 1L, "Harake Kakashi", 37, "Manager"));
        repository.add(new Employee(1L, 1L, "Gojo Satoru", 26, "Developer"));
        repository.add(new Employee(1L, 2L, "Yuji Itadori", 39, "Analyst"));
        repository.add(new Employee(1L, 2L, "Megumi Fushiguro", 27, "Developer"));
        repository.add(new Employee(2L, 3L, "Geto Suguru", 38, "Developer"));
        repository.add(new Employee(2L, 3L, "maki Zenin", 34, "Developer"));
        repository.add(new Employee(2L, 3L, "Monkey D. Luffy", 30, "Manager"));
        repository.add(new Employee(2L, 4L, "Roronoa Zoro", 25, "Developer"));
        repository.add(new Employee(2L, 4L, "Nami", 30, "Developer"));
        return repository;
    }
}
